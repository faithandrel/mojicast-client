import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';

import { CategoryPage } from '../category/category';

import { BackEndService } from '../../services/back-end-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { MojcLanguage } from '../../services/mojc-language';
import { PromptHandler } from '../../services/prompt-handler';

import { EXPLORE_EMOJI } from '../../constants';

@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html'
})
export class ExplorePage {

  emojiArray: Array<any> = [];
  extraCells: number;
  currentPage: number;
  total: number;
  counter: number = 0;

  constructor(public navCtrl: NavController, 
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private backEndService: BackEndService,
              private mojcErrorHandler: MojcErrorHandler,
              private mojcLang: MojcLanguage,
              private promptHandler: PromptHandler) { 
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present();
    this.counter    = 0;
    this.emojiArray = [];

    this.getContent(false)
        .then(res => {         
          loader.dismiss();
        })
        .catch(error => {
          loader.dismiss();
          this.mojcErrorHandler.handle(error);
        });

    if(this.showExploreEmojiPrompt()) {
      let alert = this.alertCtrl.create({
           subTitle: this.mojcLang.getTextFor(EXPLORE_EMOJI),          
           buttons: [{
                      text: 'Ok',
                      handler: () => {
                        this.promptHandler.save(EXPLORE_EMOJI);
                      }
                    }]
         });
      alert.present();  
    }
  }

  refreshPage(refresher) {
    this.counter    = 0;
    this.emojiArray = [];

    this.getContent(false)
        .then(res => {
          refresher.complete();
        })
        .catch(error => {
          refresher.complete();
          this.mojcErrorHandler.handle(error);
        });
  }

  openFeedForEmoji(emoji) {
    this.navCtrl.push(CategoryPage, { title: emoji, 
                                      type: 'emoji', 
                                      headerColor: 'light', 
                                      spinnerColor: 'grey' 
                                    });
  }

  addExtraCells(emojiRow) {
    if(emojiRow.length < 3 && this.extraCells < 3) {
      return Array.from(new Array(this.extraCells), (x,i) => i)
    }

    return [];
  }

  getContent(nextPage: boolean) {
    var page = 0;
    if(nextPage) {
      page = this.currentPage + 1;
    }

    return this.backEndService.getExploreFeed(page)
            .then(res => {
                this.currentPage = res.current_page;
                this.total       = res.total;
                this.emojiArray  = this.emojiArray.concat(res.data);
                this.extraCells  = 3-res.count%3;

                this.counter += res.count;
            })
            .catch(error => {
              this.mojcErrorHandler.handle(error);
            });
  }

  goToNextPage(infiniteScroll) {
      this.getContent(true)
            .then(res => {
              infiniteScroll.complete();
            })
            .catch(error => {
              this.mojcErrorHandler.handle(error);
            });
  }

  showExploreEmojiPrompt() {
    return !this.promptHandler.isDoneFor(EXPLORE_EMOJI);
  }

}
