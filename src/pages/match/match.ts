import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import { CategoryPage } from '../category/category';

import { BackEndService } from '../../services/back-end-service';
import { MatchingService } from '../../services/matching-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { MojcLanguage } from '../../services/mojc-language';
import { PromptHandler } from '../../services/prompt-handler';


@Component({
  selector: 'page-match',
  templateUrl: 'match.html'
})
export class MatchPage {

  

  constructor(public loadingCtrl: LoadingController,             
              private backEndService: BackEndService,
              private matchingService: MatchingService,
              private mojcErrorHandler: MojcErrorHandler,
              private mojcLang: MojcLanguage,
              private promptHandler: PromptHandler) { 

    //this.getContent(false);
  }

  /*getContent(nextPage: boolean) {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present();

    this.matchingService.getUsersForMatching(false)
        .then(res => {  
            loader.dismiss();
        })          
        .catch(error => {
            loader.dismiss();
            this.mojcErrorHandler.handle(error);
        });
    
  }*/

  userBio() {
    let user = this.matchingService.getCurrentUser();
    if (user == undefined) {
      return '';
    }
    return user.bio;
  }

  accept() {
    this.matchingService.acceptMatch();
  }

  next() {
    this.matchingService.next();  
  }

  isEmpty() {
    return this.matchingService.isEmptyList();
  }
}
