import { Component } from "@angular/core";

import { TabsPage } from '../tabs/tabs';

import { BackEndService } from '../../services/back-end-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { MojcStorage } from '../../services/mojc-storage';

import { MojcError } from '../../services/mojc-error';

import { NavController } from 'ionic-angular';


@Component({
  templateUrl: 'sign-up.html',
  selector: 'page-sign-up'
})
export class SignUpPage {

  loginUser: any;
  signUpUser: any;
  showSpinner: boolean = false;
  
  //TODO: remove instances of backEndService
  constructor(private backEndService: BackEndService,
              private mojcErrorHandler: MojcErrorHandler,
              private storage: MojcStorage,
              private navCtrl: NavController) {

    this.signUpUser = {
      username: ''
    };
  }

  signUp() {
      this.showSpinner = true;
      this.storage.getSavedUserId()
      .then(id => {  
          this.signUpUser.user = id;
          return this.backEndService.saveUsername(this.signUpUser)
      })          
      .then(res => { 
          return this.storage.setUsername(res.username);
      })    
      .then(res => { 
          this.showSpinner = false;
          this.navCtrl.setRoot(TabsPage);
      })       
      .catch(error => {
          this.showSpinner = false;
          if(error.status != undefined && error.status == 422) {
            var contents = error.json();
            if(contents.errors.username != undefined && contents.errors.username.length > 0) {
              error = new MojcError(error, contents.errors.username[0], true);
            }
          }
          this.mojcErrorHandler.handle(error);
      });
  }  
}
