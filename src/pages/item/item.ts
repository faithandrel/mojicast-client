import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, Nav, Content, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ItemService } from '../../services/item-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { MojcLocation } from '../../services/mojc-location';
import { MojcStorage } from '../../services/mojc-storage';
import { MojcLanguage } from '../../services/mojc-language';
import { PromptHandler } from '../../services/prompt-handler';

import { ChatboxPage } from '../chatbox/chatbox';

import { MojcError } from '../../services/mojc-error';

import { COMMENT_ALIAS } from '../../constants';

@Component({
  templateUrl: 'item.html',
  selector: 'page-item',
})
export class ItemPage {
  selectedItem: any;
  comments: Array<any> = [];
  commentForm: any;
  userName: any;
  userId: any;
  hideNameField: boolean = true;
  showSpinner: boolean = false;
  loadingComments: boolean = false;
  spinnerColor: string = 'black';
  @ViewChild(Content) content: Content;

  constructor(private nav: NavController, 
                      navParams: NavParams,
              private myNav: Nav,
              public alertCtrl: AlertController,
              private itemService: ItemService,
              private formBuilder: FormBuilder,
              private geolocation: Geolocation,
              private mojcLocation: MojcLocation,
              private storage: MojcStorage,
              private mojcErrorHandler: MojcErrorHandler,
              private mojcLang: MojcLanguage,
              private promptHandler: PromptHandler,
              private socialSharing: SocialSharing) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
    
    this.commentForm = this.formBuilder.group({
      name: [''],
      content: ['', Validators.required],
    });
    this.userName = this.selectedItem.viewer_name;

    this.storage.getSavedUserId()
                .then(id => {  
                    this.userId = id;
                  })          
                .catch(error => {
                    this.mojcErrorHandler.handle(error);
                });
    this.addLoadedComments(this.selectedItem.comments);
    this.storage.getLanguage().then(res => {  
                  });
  }
  
  submitComment() {
    this.showSpinner = true;

    let newItem = {
      title: '',
      content: this.commentForm.get('content').value,
      name: this.commentForm.get('name').value,
      item_id: this.selectedItem.id
    };

    this.itemService.saveItem(newItem)
    .then(res => {
      return this.itemService.getSingleItem(this.selectedItem.id);
    })
    .then(res => {
      this.selectedItem = res;
      this.comments = [];
      this.addLoadedComments(this.selectedItem.comments);
      this.commentForm.setValue({
         name:    '',
         content: ''
      });
      this.showSpinner = false;
    })
    .catch(error => {
        this.showSpinner = false;
        if(error.status != undefined && error.status == 422) {
          var contents = error.json();
          if(contents.errors.content != undefined && contents.errors.content.length > 0) {
            error = new MojcError(error, contents.errors.content[0], true);
          }
          if(contents.errors.name != undefined && contents.errors.name.length > 0) {
            error = new MojcError(error, contents.errors.name[0], true);
          }
        } //TODO: move to service
        this.mojcErrorHandler.handle(error);
    });
  }

  toggleNameField() {
    if(this.hideNameField) {
      this.hideNameField = false;
    }
    else {
      this.hideNameField = true;
    }
    if(this.showCommentAliasPrompt()) {
      let alert = this.alertCtrl.create({
           subTitle: this.mojcLang.getTextFor(COMMENT_ALIAS),          
           buttons: [{
                      text: 'Ok',
                      handler: () => {
                        this.promptHandler.save(COMMENT_ALIAS);
                      }
                    }]
         });
      alert.present();  
    }
  }

  showNameField() {
    return this.userId != this.selectedItem.user_id && this.selectedItem.has_commented == false;
  }

  getName() {
    var nameField = this.commentForm.get('name').value;

    if(nameField.length > 0) {
      return nameField;
    }

    return this.userName;
  }

  addLoadedComments(newComments) {
    for(let i = 0; i < newComments.length; i++) {
      this.comments.unshift(newComments[i]);
    }
  }

  olderComments() {
    this.loadingComments = true;
    this.itemService.getItemComments(this.selectedItem.id)
    .then(res => {
      this.loadingComments = false;
      this.addLoadedComments(res);
    })
    .catch(error => {
        this.loadingComments = false;
        this.mojcErrorHandler.handle(error);
    });
  }

  shareToFacebook() {
    this.socialSharing.shareViaFacebook('', null, 'http://mojicast.com/view/'+this.selectedItem.id)
          .then(res => {
             
          })
          .catch(error => {
            this.mojcErrorHandler.handle(error);
          });
  }

  refreshPage(refresher) {    
    this.itemService.getSingleItem(this.selectedItem.id)
        .then(res => {
            this.selectedItem = res;
            this.comments = [];
            this.addLoadedComments(this.selectedItem.comments);
            this.commentForm.setValue({
               name:    '',
               content: ''
            });
            refresher.complete();
        })
        .catch(error => {
            refresher.complete();
            if(error.status != undefined && error.status == 422) {
              var contents = error.json();
              if(contents.errors.content != undefined && contents.errors.content.length > 0) {
                error = new MojcError(error, contents.errors.content[0], true);
              }
              if(contents.errors.name != undefined && contents.errors.name.length > 0) {
                error = new MojcError(error, contents.errors.name[0], true);
              }
            } //TODO: move to service
            this.mojcErrorHandler.handle(error);
        });
  }
  
  scrollToBottom() {
    this.content.scrollToBottom();
  }

  showCommentDetails(comment) {
    comment.show_details = true;
    setTimeout(function(){
       comment.show_details = false;
    }, 5000);

  }

  showCommentChatButton(comment) {
    return comment.allow_chat && comment.show_details && this.userId != comment.user_id;
  }

  showItemChatButton() {
    return this.selectedItem.allow_chat && this.userId != this.selectedItem.user_id
  }

  formatDate(date){
    // Split timestamp into [ Y, M, D, h, m, s ]
    var t = date.split(/[- :]/);

    // Apply each element to the Date function
    var d = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));

    return d.toString().slice(0,24);
  }

  showCommentAliasPrompt() {
    return !this.promptHandler.isDoneFor(COMMENT_ALIAS);
  }
  
}
