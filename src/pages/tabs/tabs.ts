import { NotificationsPage } from './../notifications/notifications';
import { AddItemComponent } from './../../components/add-item/add-item';
import { MessageSettingsComponent } from './../../components/message-settings/message-settings';
import { ModalController, AlertController, Tabs } from 'ionic-angular';
import { ExplorePage } from './../explore/explore';
import { CategoryPage } from './../category/category';
import { MessagesPage } from './../messages/messages';
import { MatchPage } from './../match/match';
import { Component, ViewChild } from '@angular/core';

import { PromptHandler } from '../../services/prompt-handler';
import { MojcNotification } from '../../services/mojc-notification';
import { MojcLanguage } from '../../services/mojc-language';
import { MessagingService } from '../../services/messaging-service';
import { MatchingService } from '../../services/matching-service';

import { WRITE_POST, EXPLORE_EMOJI, SET_MESSAGING } from '../../constants';

@Component({
  selector: 'main-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  categoryRoot: any = CategoryPage;
  exploreRoot: any = ExplorePage;
  notificationsRoot: any = NotificationsPage;
  messagesRoot: any = MessagesPage;
  matchRoot: any = MatchPage;

  showAddButton: boolean = true;
  @ViewChild('myTabs') tabRef: Tabs;

  constructor(public modalCtrl: ModalController, 
              public alertCtrl: AlertController,
              public notif: MojcNotification,
              public message: MessagingService,
              public matching: MatchingService,
              private mojcLang: MojcLanguage,
              private promptHandler: PromptHandler) {
    
    if(this.showMessageSettingsModal()) {
      this.message.getChatSettings()
      .then(res => {
        this.openMessageSettingsModal(res);        
      })
      .catch(error => {
        //this.mojcErrorHandler.handle(error);
      });
    }
  }

  addItem() {  
    let modal = this.modalCtrl.create(AddItemComponent);
    modal.present();
    if(this.showWritePostPrompt()) {
      let alert = this.alertCtrl.create({
           subTitle: this.mojcLang.getTextFor(WRITE_POST),          
           buttons: [{
                      text: 'Ok',
                      handler: () => {
                        this.promptHandler.save(WRITE_POST);
                      }
                    }]
         });
      alert.present();  
     }
  }

  tabChange(tab) {
    this.showAddButton = true;
    if(tab.index == 3) {
      this.showAddButton = false;
    }
  }

  showWritePostPrompt() {
    return !this.promptHandler.isDoneFor(WRITE_POST);
  }

  showExploreEmojiPrompt() {
    return !this.promptHandler.isDoneFor(EXPLORE_EMOJI);
  }

  showMessageSettingsModal() {
    return !this.promptHandler.isDoneFor(SET_MESSAGING);
  }

  openMessageSettingsModal(savedSettings) {
    let alert = this.alertCtrl.create({
             subTitle: 'You need to set your message settings.',          
             buttons: [{
                        text: 'Ok',
                        handler: () => {
                          this.promptHandler.save(SET_MESSAGING);
                          let modal = this.modalCtrl.create(MessageSettingsComponent, 
                                                            {savedSettings: savedSettings, showPrompt: true});
                          modal.present();
                        }
                       }]
           });
    alert.present();
  }
}
