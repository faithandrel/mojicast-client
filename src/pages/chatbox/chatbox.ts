import { Component, ViewChild } from "@angular/core";

import { TabsPage } from '../tabs/tabs';

import { BackEndService } from '../../services/back-end-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { MojcStorage } from '../../services/mojc-storage';

import { MojcError } from '../../services/mojc-error';

import { NavController, Content, NavParams, LoadingController } from 'ionic-angular';

import * as io from 'socket.io-client';

import { CHAT_SERVER } from '../../constants';

@Component({
  templateUrl: 'chatbox.html',
  selector: 'page-chatbox'
})
export class ChatboxPage {

  socket: any;
  userId: any;
  user2Id: any;
  username: string;
  channel: string;
  message: string;
  messages: Array<any> = [];
  @ViewChild(Content) content: Content;
  
  //TODO: remove instances of backEndService
  constructor(private backEndService: BackEndService,
              private mojcErrorHandler: MojcErrorHandler,
              private storage: MojcStorage,
              public loadingCtrl: LoadingController,
              private navCtrl: NavController,
                      navParams: NavParams) {
      this.user2Id = navParams.get('user');
  }

  chatBubbleClass(user) {
    if (this.userId == user) {
      return 'talk-bubble tri-right right-top chat-user';
    }
    return 'talk-bubble tri-right left-top';
  }  

  send() {
      let message = {
        channel: this.channel,
        message: this.message,
        user: this.userId
      };

      this.socket.emit('message', message);
      this.message = '';
  }

  open() {
      let loader = this.loadingCtrl.create({content: 'Loading...'});
      loader.present();
      
      this.socket = io(CHAT_SERVER);

      this.storage.getSavedUserId()
      .then(id => {  
            this.userId = id;
            return this.backEndService.openChatbox(this.user2Id);
      })         
      .then(res => {  
            for(let i=0; i<res.sent_messages.length; i++) {
              this.messages.push({
                message: res.sent_messages[i].message,
                user: res.sent_messages[i].user_id,
                date: res.sent_messages[i].created_at
              });
            }
            this.channel = res.channel;
            this.username = res.username;
            this.receive(this.channel);
            this.socket.emit('open-chatbox', {
                                                chatboxId: res.id,
                                                channel: this.channel,
                                                user: this.userId,
                                                username: res.username,
                                                status: res.status,
                                                users: {
                                                  user: res.user_id,
                                                  user2: res.user2_id
                                                }
                                             });
            loader.dismiss();
            let content = this.content;
            setTimeout(function() {
               content.scrollToBottom();
            }, 200);
      })          
      .catch(error => {
            loader.dismiss();
            this.mojcErrorHandler.handle(error);
      });
  }

  receive(channel) {
      this.socket.on(channel, (message) => {        
        if (Array.isArray(message)) {
          for (var i=0; i<message.length; i++) {
            let previousMessage = message[i];
            this.messages.push(previousMessage);
          }
        }
        else {
          this.messages.push(message);
        }

        let content = this.content;
        setTimeout(function(){
           content.scrollToBottom();
        }, 300);
      });
  }

  showMessageDate(message) {
    message.show_date = true;
    setTimeout(function(){
       message.show_date = false;
    }, 3000);
  }

  close() {
      this.socket.emit('close-chatbox', { channel: this.channel, user: this.userId });
      this.userId = 0;
      //this.user2Id = 0; so user can re enter previously opened chatbox
      this.username = '';
      this.channel = '';
      this.message = '';
      this.messages = [];
  }

  ionViewDidEnter() {
      this.open();
  }

  ionViewDidLeave() {
      this.close();
  }
}
