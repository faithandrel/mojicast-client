import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { ExplorePage } from './../explore/explore';
import { ItemService } from '../../services/item-service';
import { MessagingService } from '../../services/messaging-service';
import { MatchingService } from '../../services/matching-service';
import { MojcNotification } from '../../services/mojc-notification';
import { PromptHandler } from '../../services/prompt-handler';
import { MojcErrorHandler } from '../../services/mojc-error-handler';

import { 
  ItemFCMNotificaton,
  ChatRequestNotification,
  NewMatchNotification
} from '../../services/mojc-fcm-notifications';

import { 
  WRITE_POST,
  NEW_COMMENT,
  VIEW_COUNT,
  CHAT_REQUEST,
  NEW_MATCH,
  NEW_MESSAGE,
  CONFIRMED_CHAT
} from '../../constants';

import { Subscription } from 'rxjs';


@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})
export class CategoryPage {

  feedTitle: string = 'Near You';
  feedType: string = 'news';
  headerColor: string = 'primary';
  backEndFunction: any;
  items: any;
  showSpinner: boolean = false;
  spinnerColor: string = 'white';
  newItemSubscription: Subscription;
  postedItems: Array<any> = [];

  constructor(private navCtrl: NavController, 
                      navParams: NavParams, 
              private modalCtrl: ModalController,
              private itemService: ItemService,
              private messagingService: MessagingService,
              private matchingService: MatchingService,
              private notification: MojcNotification,
              private mojcErrorHandler: MojcErrorHandler) { 
    
    let feedTitle = navParams.get('title');
    let feedType  = navParams.get('type');
    let headerColor  = navParams.get('headerColor');
    let spinnerColor = navParams.get('spinnerColor');

    if(feedTitle != undefined && feedType != undefined) {
      this.feedTitle = feedTitle;
      this.feedType  = feedType;
    }

    if(headerColor != undefined) {
      this.headerColor = headerColor;
    }

    if(spinnerColor != undefined) {
      this.spinnerColor = spinnerColor;
    }

    this.newItemSubscription = this.itemService.onItemPost
            .subscribe(item=>{
              this.postedItems.push(item);
            });

    this.showSpinner = true;

    this.getContent()
        .then(res => {
          this.showSpinner = false;
        })
        .catch(error => {
          this.showSpinner = false;
          this.mojcErrorHandler.handle(error);
        });
    
    let fcmNotification = this.notification.getFCMNotification();

    if (fcmNotification) {
      if(fcmNotification.notification_type == NEW_COMMENT.type ||
         fcmNotification.notification_type == VIEW_COUNT.type ) {
        let action = new ItemFCMNotificaton(fcmNotification.item, this.navCtrl);
        action.openFCMNotification();
      }
      
      if(fcmNotification.notification_type == CHAT_REQUEST.type || 
         fcmNotification.notification_type == NEW_MESSAGE.type || 
         fcmNotification.notification_type == CONFIRMED_CHAT.type) {
        let action = new ChatRequestNotification(fcmNotification.details, 
                                                 this.navCtrl, 
                                                 this.modalCtrl, 
                                                 this.messagingService);
        action.openFCMNotification();
      }
      
      if(fcmNotification.notification_type == NEW_MATCH.type) {
        let action = new NewMatchNotification(this.matchingService, 
                                                 this.modalCtrl, 
                                                 fcmNotification.match, 
                                                 fcmNotification.user);
        action.openFCMNotification();
      }
    }
  }

  getContent() {
    return this.callBackEndFunction(false)
          .then(res => {
            this.postedItems = [];
            this.items = res;
          })
          .catch(error => {
            this.mojcErrorHandler.handle(error);
          });
  }

  refreshPage(refresher) {
    this.getContent()
        .then(res => {
           refresher.complete();
        })
        .catch(error => {
          refresher.complete();
          this.mojcErrorHandler.handle(error);
        });
  }

  goToNextPage(infiniteScroll) {
    this.callBackEndFunction(true)
          .then(res => {
            for (let i = 0; i < res.length; i++) {
              this.items.push(res[i]);
            }
          })
          .catch(error => {
            this.mojcErrorHandler.handle(error);
          })
          .then(res => {
             infiniteScroll.complete();
          });
  }

  callBackEndFunction(nextPage: boolean) {
    if(this.feedType == 'news') {
      return this.itemService.getNewsItems(nextPage);
    }
    if(this.feedType == 'emoji') {
      return this.itemService.getEmojiItems(this.feedTitle, nextPage);
    }
    if(this.feedType == 'user') {
      return this.itemService.getUserItems(nextPage);
    }
  }

  getPostedItems(): Array<any> {
    if(this.feedType == 'news') {
      return this.postedItems;
    }
    return [];
  }

  openExplorePage() {
    this.navCtrl.push(ExplorePage);
  }
}
