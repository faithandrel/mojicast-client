import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController } from 'ionic-angular';
import { ItemPage } from '../../pages/item/item';
import { ChatboxPage } from '../../pages/chatbox/chatbox';
import { CategoryPage } from '../category/category';
import { ProfileComponent } from './../../components/profile/profile';
import { ConfirmChatboxComponent } from './../../components/confirm-chatbox/confirm-chatbox';
import { OpenMatchComponent } from './../../components/open-match/open-match';
import { ItemService } from '../../services/item-service';
import { MessagingService } from '../../services/messaging-service';
import { MatchingService } from '../../services/matching-service';
import { MojcNotification } from '../../services/mojc-notification';
import { MojcErrorHandler } from '../../services/mojc-error-handler';

import { MojcError } from '../../services/mojc-error';

import {
	NEW_COMMENT,
	VIEW_COUNT,
	CHAT_REQUEST,
	NEW_MATCH,
	NEW_MESSAGE,
	CONFIRMED_CHAT,
	PENDING_CHATBOX_STATUS,
	ACTIVE_CHATBOX_STATUS,
	DECLINED_CHATBOX_STATUS,
	PENDING_MATCH_STATUS
} from '../../constants';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})
export class NotificationsPage {

  	constructor(public navCtrl: NavController,
  				public loadingCtrl: LoadingController,
  				public modalCtrl: ModalController,
  			  private itemService: ItemService,
  			  private messagingService: MessagingService,
  			  private matchingService: MatchingService,
  			  private notification: MojcNotification,
  			  private mojcErrorHandler: MojcErrorHandler
  			  ) { }

	ionViewDidEnter() {
	    this.notification
	        .markAsRead()
	        .then(res => {  
	            
	         })          
	        .catch(error => {
	            this.mojcErrorHandler.handle(error);
	        });
	}
	

	ionViewDidLeave() {
		this.notification.clearNewNotifications();
	}

	getNotifications() {
	  	this.notification
	        .getNewNotifications()
	        .then(res => {  
	            
	         })          
	        .catch(error => {
	            this.mojcErrorHandler.handle(error);
	        });
	}

	getContent() {
		return this.notification.getAllNotifications()
		        .then(res => { 
		          return this.notification.markAsRead();
		        })          
		        .catch(error => {
		            this.mojcErrorHandler.handle(error);
		        });
	}

	refreshPage(refresher) {
		this.notification.clearNewNotifications();
    	this.getContent()
	        .then(res => {
	           refresher.complete();
	        })
	        .catch(error => {
	          refresher.complete();
	          this.mojcErrorHandler.handle(error);
	        });
  	}

  	openNotification(notification) {
  		if(notification.type == NEW_COMMENT.type || notification.type == VIEW_COUNT.type) {
  			this.viewSingleItem(notification.item_id);
  		}
  		if(notification.type == CHAT_REQUEST.type || notification.type == NEW_MESSAGE.type || 
  		   notification.type == CONFIRMED_CHAT.type) {
  			this.openChatRequest(notification);
  		}
  		if(notification.type == NEW_MATCH.type) {
  			this.openMatch(notification.match_id);
  		}
  	}

  	viewSingleItem(itemId) {
	    let loader = this.loadingCtrl.create({content: 'Loading...'});
	    loader.present()
	    this.itemService
	        .getSingleItem(itemId)
	        .then(res => {
	            this.navCtrl.push(ItemPage, { item: res });
	            loader.dismiss();
	        })
	        .catch(error => {
	        	loader.dismiss();
	         	this.mojcErrorHandler.handle(error);
	        });
    }

    openChatRequest(notification) {
    	let chatboxId = notification.chatbox_id;
    	let fromMatch = notification.match_id ? true : false;
    	
    	let loader = this.loadingCtrl.create({content: 'Loading...'});
	    loader.present()
    	this.messagingService.getChatboxDetails(chatboxId)
    		.then(res => {
    			loader.dismiss();
    			if(res.status == PENDING_CHATBOX_STATUS) {
	            	let modal = this.modalCtrl.create(ConfirmChatboxComponent, {details: res, 
	            																chatbox: chatboxId,
	            																from_match: fromMatch});
	            	modal.present();
    			}
    			if(res.status == ACTIVE_CHATBOX_STATUS) {
    				this.navCtrl.push(ChatboxPage, { user: res.user_id });
    			}
    			if(res.status == DECLINED_CHATBOX_STATUS) {
    				this.messagingService.showDeclinedAlert();
    			}
	        })
	        .catch(error => {
	        	loader.dismiss();
	        	if(error.status != undefined && error.status == 403) {
					var contents = error.json();
					if(contents.message != undefined && contents.message.length > 0) {
						error = new MojcError(error, contents.message, true);
					}
			     }
	         	this.mojcErrorHandler.handle(error);
	        });
    }

    openMatch(matchId) {
    	let loader = this.loadingCtrl.create({content: 'Loading...'});
    	let match = null;
	    loader.present()
	    
	    this.matchingService.getMatch(matchId)
	    .then(res => {
	    	match = res;
	        return this.matchingService.getUserBio(res.user2_id);
	     })
	    .then(res => {
	    	loader.dismiss();	  

	    	if(match.status != PENDING_MATCH_STATUS) {
	    		this.matchingService.showOpenedAlert();
	    	} 
	    	else {
	  			let modal = this.modalCtrl.create(OpenMatchComponent, {match: matchId, user: res});
		        modal.present();
		    }
	    })
	    .catch(error => {
	        loader.dismiss();
	        this.mojcErrorHandler.handle(error);
	    });
    }

    openUserItems() {
		this.navCtrl.push(CategoryPage, { title: 'Your stories', type: 'user' });
	}

	openProfile() {
		let modal = this.modalCtrl.create(ProfileComponent);
    	modal.present();
	}

	goToNextPage(infiniteScroll) {
	    this.notification.getOldNotifications(true)
	          .then(res => {
	            infiniteScroll.complete();
	          })
	          .catch(error => {
	            this.mojcErrorHandler.handle(error);
	          });
	  }
}
