import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController } from 'ionic-angular';
import { MessagingService } from '../../services/messaging-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { ChatboxPage } from '../../pages/chatbox/chatbox';
import { MessageSettingsComponent } from './../../components/message-settings/message-settings';
import { ChatroomGuidelinesComponent } from './../../components/chatroom-guidelines/chatroom-guidelines';

@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html'
})
export class MessagesPage {

	chatboxes: any = [];
	total: number = 0;

  	constructor(public navCtrl: NavController,
  				public loadingCtrl: LoadingController,
  				public modalCtrl: ModalController,
  			  private messagingService: MessagingService,
  			  private mojcErrorHandler: MojcErrorHandler
  			  ) {  		
  	}

	getContent() {
		return this.messagingService.getChatboxes(false)
		        .then(res => { 
		          this.chatboxes = res.chatboxes;
		          this.total = res.total;
		        })          
		        .catch(error => {
		            this.mojcErrorHandler.handle(error);
		        });
	}

	openMessage(chatbox, index) {
		if(this.chatboxes[index].unread) {
			this.messagingService.decrementCounter(this.chatboxes[index].unread_count);
			this.chatboxes[index].unread = false;
		}
		
  		this.navCtrl.push(ChatboxPage, { user: chatbox.to_user });
  	}

	refreshPage(refresher) {
    	this.getContent()
	        .then(res => {
	           refresher.complete();
	        })
	        .catch(error => {
	          refresher.complete();
	          this.mojcErrorHandler.handle(error);
	        });
  	}

	goToNextPage(infiniteScroll) {
	    this.messagingService.getChatboxes(true)
	          .then(res => {
	          	this.chatboxes = res.chatboxes;
		        this.total = res.total;
	            infiniteScroll.complete();
	          })
	          .catch(error => {
	            this.mojcErrorHandler.handle(error);
	          });
	}

	openSettings() {
		/*let savedSettings = {};
		let loader = this.loadingCtrl.create({content: 'Loading...'});
		loader.present()


		this.messagingService.getChatSettings()
		.then(res => {
			savedSettings = res;
			loader.dismiss();
			return Promise.resolve(res);
		})
		.then(res => {
			let modal = this.modalCtrl.create(MessageSettingsComponent, {savedSettings: savedSettings});
			modal.present();
		})
		.catch(error => {
			loader.dismiss();
			this.mojcErrorHandler.handle(error);
		});*/

		let modal = this.modalCtrl.create(ChatroomGuidelinesComponent);
		modal.present();
		
	}

	ionViewDidEnter() {
	    this.chatboxes = this.messagingService.chatboxes;
	  	this.total = this.messagingService.total;
	  	this.messagingService.turnOffMessageChecks();
  	}

  	ionViewDidLeave() {
    	this.messagingService.getMessagesOnInterval();
  	}
}
