import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { IonicStorageModule } from '@ionic/storage';
import { Facebook } from '@ionic-native/facebook';
import { Geolocation } from '@ionic-native/geolocation';
import { FCM } from '@ionic-native/fcm';
import { Badge } from '@ionic-native/badge';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';
import { WebIntent } from '@ionic-native/web-intent';
import { SocialSharing } from '@ionic-native/social-sharing';

import { LogInPage}  from '../pages/log-in/log-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { ItemPage } from '../pages/item/item';
import { ExplorePage } from '../pages/explore/explore';
import { CategoryPage } from './../pages/category/category';
import { NotificationsPage } from './../pages/notifications/notifications';
import { TabsPage } from '../pages/tabs/tabs';
import { ChatboxPage } from '../pages/chatbox/chatbox';
import { MessagesPage } from '../pages/messages/messages';
import { MatchPage } from '../pages/match/match';

import { BackEndService } from '../services/back-end-service';
import { ItemService } from '../services/item-service';
import { MessagingService } from '../services/messaging-service';
import { MatchingService } from '../services/matching-service';
import { MojcErrorHandler } from '../services/mojc-error-handler';
import { MojcLocation } from '../services/mojc-location';
import { MojcFacebook } from '../services/mojc-facebook';
import { MojcStorage } from '../services/mojc-storage';
import { MojcNotification } from '../services/mojc-notification';
import { MojcConfiguration } from '../services/mojc-configuration';
import { MojcLanguage } from '../services/mojc-language';
import { PromptHandler } from '../services/prompt-handler';

import { AddItemComponent } from './../components/add-item/add-item';
import { FeedComponent } from './../components/feed-section/feed-section';
import { SpinnerComponent } from './../components/mojc-spinner/mojc-spinner';
import { ProfileComponent } from './../components/profile/profile';
import { OpenChatboxComponent } from './../components/open-chatbox/open-chatbox';
import { MessageSettingsComponent } from './../components/message-settings/message-settings';
import { UserBioComponent } from './../components/user-bio/user-bio';
import { ConfirmChatboxComponent } from './../components/confirm-chatbox/confirm-chatbox';
import { OpenMatchComponent } from './../components/open-match/open-match';
import { ChatroomGuidelinesComponent } from './../components/chatroom-guidelines/chatroom-guidelines';

@NgModule({
  declarations: [
    MyApp,
    LogInPage,
    SignUpPage,
    ItemPage,
    ExplorePage,
    CategoryPage,
    TabsPage,
    NotificationsPage,
    ChatboxPage,
    MessagesPage,
    MatchPage,
    AddItemComponent,
    FeedComponent,
    SpinnerComponent,
    ProfileComponent,
    OpenChatboxComponent,
    MessageSettingsComponent,
    UserBioComponent,
    ConfirmChatboxComponent,
    OpenMatchComponent,
    ChatroomGuidelinesComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LogInPage,
    SignUpPage,
    ItemPage,
    ExplorePage,
    CategoryPage,
    TabsPage,
    NotificationsPage,
    ChatboxPage,
    MessagesPage,
    MatchPage,
    AddItemComponent,
    FeedComponent,
    SpinnerComponent,
    ProfileComponent,
    OpenChatboxComponent,
    MessageSettingsComponent,
    UserBioComponent,
    ConfirmChatboxComponent,
    OpenMatchComponent,
    ChatroomGuidelinesComponent
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},
              {provide: BackEndService, useClass: BackEndService},
              {provide: ItemService, useClass: ItemService},
              {provide: MessagingService, useClass: MessagingService},
              {provide: MatchingService, useClass: MatchingService},
              {provide: MojcErrorHandler, useClass: MojcErrorHandler},
              {provide: MojcLocation, useClass: MojcLocation},
              {provide: Facebook, useClass: Facebook},
              {provide: Geolocation, useClass: Geolocation},
              {provide: FCM, useClass: FCM},
              {provide: Badge, useClass: Badge},
              {provide: Diagnostic, useClass: Diagnostic},
              {provide: Network, useClass: Network},
              {provide: WebIntent, useClass: WebIntent},
              {provide: SocialSharing, useClass: SocialSharing},
              {provide: MojcFacebook, useClass: MojcFacebook},
              {provide: MojcStorage, useClass: MojcStorage},
              {provide: MojcNotification, useClass: MojcNotification},
              {provide: MojcConfiguration, useClass: MojcConfiguration},
              {provide: MojcLanguage, useClass: MojcLanguage},
              {provide: PromptHandler, useClass: PromptHandler}]
})
export class AppModule {}
