import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { LogInPage } from '../pages/log-in/log-in';

import { MojcLocation } from '../services/mojc-location';
import { MojcErrorHandler } from '../services/mojc-error-handler';
import { MojcStorage } from '../services/mojc-storage';
import { MojcConfiguration } from '../services/mojc-configuration';
import { MessagingService } from '../services/messaging-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
 
  rootPage: any;
  pages: Array<{title: string, component: any}>

  constructor(private platform: Platform,
              private mojcErrorHandler: MojcErrorHandler,
              private storage: MojcStorage,
              private mojcConfig: MojcConfiguration,
              private messaging: MessagingService,
              public alertCtrl: AlertController) {
    this.initializeApp();
    
    this.rootPage = LogInPage;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      Splashscreen.hide();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      StatusBar.styleDefault();

      this.mojcErrorHandler.onUnauthorizedRequest
      .subscribe(res=>{
          let alert = this.alertCtrl.create({
           title: 'You are not logged in!',          
           buttons: [{
                      text: 'Ok',
                      handler: () => {
                        this.logout();
                      }
                    }]
         });
         alert.present();  
      });
    });
  }

  logout() {
     this.storage.clearUser()
                .then(res => {  
                    this.mojcConfig.turnOffIntervalChecks();
                    this.messaging.turnOffMessageChecks();
                    this.nav.setRoot(LogInPage);
                  })          
                .catch(error => {
                    this.mojcErrorHandler.handle(error);
                });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
