import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

import { BackEndService } from './back-end-service';

@Injectable()
export class MatchingService {

	currentPage: number = 0;
	currentIndex: number = 0;
	lastPage: number;
	perPage: number;
	total: number;
	counter: number = 0;
	userList: Array<any> = [];
	accepted: Array<any> = [];

	constructor(private backEndService: BackEndService,  
				public alertCtrl: AlertController) {

	}

  	accept(matchId): Promise<any> {
  		return this.backEndService.sendMatchResponse(matchId, true);
  	}

  	decline(matchId): Promise<any> {
  		return this.backEndService.sendMatchResponse(matchId, false);
  	}

  	getMatch(matchId): Promise<any> {
		return this.backEndService.getMatch(matchId);
	}

	getUserBio(userId): Promise<any> {
		return this.backEndService.getUserBio(userId);
	}

	showOpenedAlert() {
		let alert = this.alertCtrl.create({
		             subTitle: 'You already responded to this match.',          
		             buttons: [{
		                        text: 'Ok',
		                        handler: () => {
		                         
		                        }
		                       }]
		           });
		alert.present();
	}

	getUsersForMatching(nextPage: boolean): Promise<any> {
		let page = this.currentPage;
		if(nextPage) {
			page += 1;
			if(this.currentPage == this.lastPage) {
				page = 0;
			}			
		}

		this.currentIndex = 0;

		return this.backEndService.getActiveUsers(page)
            .then(res => {
                this.currentPage = res.current_page;
                this.total       = res.total;
                this.lastPage    = res.last_page;
                this.perPage     = res.per_page;
                this.userList    = res.data.filter(user => {
                						return this.accepted.indexOf(user.id) == -1;
                					}); 
                console.log('user list: t'+this.userList.length);
                this.counter     = this.userList.length;        
            })
            .catch(this.handleError);
	}

	getCurrentUser() {
		return this.userList[this.currentIndex];
	}

	acceptMatch() {
		let acceptedUser = this.userList[this.currentIndex];

		return this.backEndService.openChatFromMatch(acceptedUser.id)
            .then(res => {
            	this.accepted.push(acceptedUser.id);
				this.userList.splice(this.currentIndex, 1);

				if(this.currentIndex >= this.userList.length) {
					this.currentIndex = 0;
				}

				this.decrementCounter();
				console.log(this.currentIndex);
				console.log(this.userList);
				console.log(this.accepted);
				console.log('counter: '+this.counter);
            })
            .catch(this.handleError);
	}

	next() {
		let nextIndex = this.currentIndex + 1;
		if (nextIndex < this.userList.length) {
			this.currentIndex++;
		}
		else {
			this.currentIndex = 0;
		}

		this.decrementCounter();
		console.log(this.userList);
		console.log(this.accepted);
		console.log('counter: '+this.counter);
	}

	decrementCounter() {
		if(this.counter > 0) {
			this.counter--;
		}
	}

	badgeDisplay() {
		if (this.counter < 1) {
			return 0;
		}
		if (this.counter > 10) {
			return '9+';
		}
		return this.counter.toString();
	}

	checkUsersList(): Promise<any> {
		if (this.counter > 0) {
			return Promise.resolve();
		}
		return this.getUsersForMatching(true);
	}

	isEmptyList() {
		return this.userList.length < 1;
	}

	private handleError(error: any) {
    	return Promise.reject(error.message || error);
	}
}