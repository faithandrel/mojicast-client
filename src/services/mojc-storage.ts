import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';

@Injectable()
export class MojcStorage {

	jwtToken: any = null;
	jwtHelper: JwtHelper = new JwtHelper();
	sendJwtRefresh: boolean = false;
	secondsLeftToRefresh: number = 129600;

	constructor(private storage: Storage) {
	}

	getSavedJwt() {
		return this.storage.ready().then(() => {
	       return this.storage.get('id_token');
	    })
	    .then(res => {  
	    	this.jwtToken = res;
            return res;
        })
	    .catch(this.handleError)
	    
	}

	getSavedUsername(): Promise<any> {
		return this.storage.ready().then(() => {
	       return this.storage.get('user_name');
	    })
	    .catch(this.handleError)
	}

	getSavedUserId(): Promise<any> {
		return this.storage.ready().then(() => {
	       return this.storage.get('user_id');
	    })
	    .catch(this.handleError)
	}

	setUsername(username): Promise<any> {
		return this.storage.ready().then(() => {
	       this.storage.set('user_name', username);
	    })
	    .catch(this.handleError)
	}

	setJwt(token, payload): Promise<any> {
		this.jwtToken = token;
		return this.storage.ready().then(() => {
		    this.storage.set('id_token', token);
		    this.storage.set('user_name', payload.name);
		    this.storage.set('user_id', payload.id);
		    return Promise.resolve(true);
		})
		.catch(this.handleError)
	}

	clearUser(): Promise<any> {
		return this.storage.ready().then(() => {
			this.jwtToken = null;
	    	return this.storage.remove('id_token');
	    })
	    .then(() => {
	    	return this.storage.remove('user_name');
	    })
	    .then(() => {
	    	return this.storage.remove('user_id');
	    })
	    .catch(this.handleError)
	}

	isLoggedIn() {
    	return tokenNotExpired('id_token', this.jwtToken);
	}

	getExpiryDate() {
	    if(this.jwtToken !== null) {
	     	return this.jwtHelper.getTokenExpirationDate(this.jwtToken);
	    }
	    return null;
 	}

	getSecondsBeforeTokenExpires(): number {
		var expiry = this.getExpiryDate();
		var now    = Date.now();
		
		var milliseconds = expiry.getTime() -  now;
		var seconds      = Math.floor((milliseconds) / (1000));
		
		return seconds;
	}

	checkTokenExpiry() {
		var seconds = this.getSecondsBeforeTokenExpires();
		if(seconds < this.secondsLeftToRefresh) {
			this.sendJwtRefresh = true;
		}
	}

	savePrompts(settings: Array<string>): Promise<any>{
		return this.storage.ready().then(() => {
		    this.storage.set('prompt_settings', settings);
		})
		.catch(this.handleError)
	}

	getPrompts(): Promise<any> {
		return this.storage.ready().then(() => {
	       return this.storage.get('prompt_settings');
	    })
	    .catch(this.handleError)
	}

	setLanguage(language): Promise<any> {
		return this.storage.ready().then(() => {
	       this.storage.set('language', language);
	    })
	    .catch(this.handleError)
	}

	getLanguage(): Promise<any> {
		return this.storage.ready().then(() => {
	       return this.storage.get('language');
	    })
	    .catch(this.handleError)
	}

	private handleError(error: any) {
	    return Promise.reject(error.message || error);
	 }
  
}