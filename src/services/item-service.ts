import { Injectable } from '@angular/core';
import { BackEndService } from './back-end-service';
import { MojcLocation } from './mojc-location';

import { Subject } from 'rxjs/Subject';

@Injectable()
export class ItemService {

  currentNewsFeedPage: number;
  currentEmojiFeedPage: number;
  currentCommentPage: number;
  currentUserItemsPage: number;
  pendingItems: Array<number> = [];
  newsItems: Array<any> = [];
  emojiItems: Array<any> = [];
  pendingLocationFlag: boolean = false;
  onItemPost: any;


  constructor(private backEndService: BackEndService,
              private mojcLocation: MojcLocation) {
    this.onItemPost = new Subject();
  }
  
  getSingleItem(itemId): Promise<any> {
    return this.backEndService
          .getSingleItem(itemId)
          .then(res => {
              this.currentCommentPage = 1;
              return res;
          })
          .catch(error => {
            return Promise.reject(error);
          });
  }

  saveItem(newItem): Promise<any> {
    var geoAvailable = false;
    return this.mojcLocation.getGeo()
            .then(res => {
                let item = Object.assign(newItem, res);
                geoAvailable = true;
                return this.backEndService.saveItem(item);
            })
            .catch(error => {
              return this.backEndService.saveItem(newItem)
            })
            .then(res => {
              if(!geoAvailable) {
                this.pendingItems.push(res.id);
              }
              if(res.item_id == undefined || res.item_id == null || res.item_id < 1) {
                this.onItemPost.next(res);
              }
              return res;
            })
            .catch(error => {
              return Promise.reject(error);
            });
  }

  getItemComments(itemId): Promise<any> {
    var page = this.currentCommentPage + 1;
    return this.backEndService
          .getItemComments(itemId, page)
          .then(res => {
            this.currentCommentPage = res.current_page;
            return Promise.resolve(res.data);
          })
          .catch(error => {
            return Promise.reject(error);
          });
  } 

  saveItemLocation(latitude: number, longitude: number): Promise<any> {
    if(this.pendingItems.length < 1) {
      return Promise.resolve([]);
    }

    return this.backEndService
          .sendPendingLocation(this.pendingItems, latitude, longitude)
          .then(res => {
              this.pendingItems = [];
              return Promise.resolve(res);
          })
          .catch(error => {
            return Promise.reject(error);
          });
  }

  getItemsWithoutLocation(): Promise<any> {
    return this.backEndService
          .getItemsWithoutLocation()
          .then(res => {
              this.pendingItems = res;
          })
          .catch(error => {
            return Promise.reject(error);
          });
  } 

  getNewsItems(nextPage: boolean): Promise<any> {
    var page = 0;
    if(nextPage) {
      page = this.currentNewsFeedPage + 1;
    }

    return this.mojcLocation.getGeo()
            .then(res => {
              return this.backEndService.getItems(res, page);
            }) 
            .catch(error => {
              this.pendingLocationFlag = true;
              return this.backEndService.getItems(null, page);
            })
            .then(res => {
              this.currentNewsFeedPage = res.current_page;
              return Promise.resolve(res.data);
            })
            .catch(error => {
              return Promise.reject(error);
            });
  }

  getEmojiItems(emoji, nextPage: boolean): Promise<any> {
    var page = 0;
    if(nextPage) {
      page = this.currentEmojiFeedPage + 1;
    }

    return this.mojcLocation.getGeo()
            .then(res => {
              return this.backEndService.getEmojiFeed(emoji, res, page);
            }) 
            .catch(error => {
              this.pendingLocationFlag = true;
              return this.backEndService.getEmojiFeed(emoji, null, page);
            })
            .then(res => {
              this.currentEmojiFeedPage = res.current_page;
              return Promise.resolve(res.data);
            })
            .catch(error => {
              return Promise.reject(error);
            });
  }

  getUserItems(nextPage: boolean): Promise<any> {
    var page = 0;
    if(nextPage) {
      page = this.currentUserItemsPage + 1;
    }

    return this.backEndService.getUserItems(page)
            .then(res => {
              this.currentUserItemsPage = res.current_page;
              return Promise.resolve(res.data);
            })
            .catch(error => {
              return Promise.reject(error);
            });
  }
}