import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';
import { WebIntent } from '@ionic-native/web-intent';
import { Badge } from '@ionic-native/badge';

import { BackEndService } from './back-end-service';
import { MessagingService } from './messaging-service';
import { MatchingService } from './matching-service';

import { 
	NEW_COMMENT,
	VIEW_COUNT,
	CHAT_REQUEST,
	NEW_MATCH,
	NEW_MESSAGE,
	CONFIRMED_CHAT
} from '../constants';

@Injectable()
export class MojcNotification {
  	
  	counter: number = 0; //TODO: make observable?
	oldNotifications: Array<any> = [];
	newNotifications: Array<any> = [];
	readNotifications: Array<any> = [];
	readNotifTags: Array<any> = [];
	currentPage: number;
	total: number;
	fcmNotification: any = null;
	shownFcm: boolean = false;

	constructor(private backEndService: BackEndService,
				private messagingService: MessagingService,
				private matchingService: MatchingService,
				public toastCtrl: ToastController,
				public fcm: FCM,
				private webIntent: WebIntent,
				private badge: Badge) {
	}

	getDeviceToken(): Promise<any> {
		return this.fcm.getToken();
	}

	initializeFCMObserver() {	    
	    return this.fcm.onNotification()
	    			.subscribe(data=>{	 
						if(data.type == 'text') {
							let toast = this.toastCtrl.create({
									message: data.text,
									duration: 3000,
							    });			    
							toast.present();
							return;
						}
				    });
	  }

	checkNotificationFromTray(): Promise<any> {
		//let fcmNotification = null;
		return this.webIntent
		        .getIntent()
		        .then(res => {  
		        	if(res.extras == undefined) {
		        		return Promise.resolve(false);
		        	}
		        	this.fcmNotification = res.extras;     	
		            
		            if(res.extras.tag !== undefined) {
		            	this.markOneAsRead(res.extras.tag);
		            	
		            	if(this.fcmNotification.notification_type == NEW_COMMENT.type ||
		            	   this.fcmNotification.notification_type == VIEW_COUNT.type	) {

		            		return this.backEndService.getSingleItem(res.extras.item);
		            	}

		            	if(this.fcmNotification.notification_type == CHAT_REQUEST.type || 
		            	   this.fcmNotification.notification_type == NEW_MESSAGE.type || 
		            	   this.fcmNotification.notification_type == CONFIRMED_CHAT.type) {

		            		this.fcmNotification.chatboxId = res.extras.chatbox;
		            		return this.messagingService.getChatboxDetails(res.extras.chatbox)
		            	}

		            	if(this.fcmNotification.notification_type == NEW_MATCH.type) {
		            		
		            		return this.matchingService.getMatch(res.extras.match)
								    .then(res => {
								    	this.fcmNotification.match = res;
								        return this.matchingService.getUserBio(res.user2_id);
								     })
		            	}


		            	return Promise.resolve(res.extras);
		            }
		            return Promise.resolve(false);
		        })
		        .then(res => {  
		        	if(this.fcmNotification == null) {
		        		return Promise.resolve(false);
		        	}

		        	if(this.fcmNotification.notification_type == NEW_COMMENT.type ||
		              this. fcmNotification.notification_type == VIEW_COUNT.type	) {
		            	
		            	this.fcmNotification.item = res;		            	
		            }
		            if(this.fcmNotification.notification_type == CHAT_REQUEST.type || 
		               this.fcmNotification.notification_type == NEW_MESSAGE.type || 
		               this.fcmNotification.notification_type == CONFIRMED_CHAT.type) {
	            		
	            		this.fcmNotification.details = res;
	            		this.fcmNotification.details.chatboxId = this.fcmNotification.chatboxId;
	            	}

	            	if(this.fcmNotification.notification_type == NEW_MATCH.type) {
	            		
	            		this.fcmNotification.user = res;
	            	}

		            return Promise.resolve(this.fcmNotification)
		        })
		        .catch(this.handleError); //TODO: 403 errors interrupts log in
	}

	getFCMNotification() {
		if(this.fcmNotification != null && !this.shownFcm) {
			this.shownFcm = true;
			return this.fcmNotification;
		}
		return false;
	}

	getAllNotifications(): Promise<any> {
	  	return this.backEndService
	        .getAllNotifications()
	        .then(res => {  
	        	if(res.new == undefined) {
	        		return;
	        	} 	
	        	this.counter 	      = res.new.length;              
	            this.newNotifications = res.new;
	            this.oldNotifications = res.old.data;
	            this.currentPage      = res.old.current_page;
	            this.total            = res.old.total;
	            this.setIconBadge();
	            
	            return res;
	         })  
          	.catch(this.handleError);
	}

	getNewNotifications(): Promise<any> {
	  	return this.backEndService
	        .getNewNotifications()
	        .then(res => {
	        	this.counter 	      = res.length;  	           	
	            this.newNotifications = res;
	            this.setIconBadge();
	            return res;
	         })  
          	.catch(this.handleError);
	}

	getOldNotifications(nextPage: boolean): Promise<any> {
	    var page = 0;
	    if(nextPage) {
	      page = this.currentPage + 1;
	    }

	    return this.backEndService.getOldNotifications(this.readNotifTags, page)
	            .then(res => {
					this.currentPage = res.current_page;
					this.total       = res.total;
					for(var i=0; i < res.data.length; i++) {
						this.oldNotifications.push(res.data[i]);
					}
	              	return Promise.resolve(res.data);
	            })
	            .catch(error => {
	              	return Promise.reject(error);
	            });
	  }

	markAsRead(): Promise<any> {
		var readNotifs = [];

		for(var key in this.newNotifications) {
			//check that notif not in old
			var currentNotification = this.newNotifications[key];
			var found = this.oldNotifications.find(function(notification) {
			  return notification.tag == currentNotification.tag;
			}, currentNotification);

			if(found == undefined) {
				currentNotification.read_at = Date.now();
				readNotifs.push(currentNotification.tag);
				this.readNotifications.push(currentNotification);
			}
		}

		return this.backEndService
	        .sendReadNotifications(readNotifs) 
	        .then(res => {  	           	
	            this.counter = 0;
	            this.setIconBadge();
	            this.readNotifTags = readNotifs;
	         })  
          	.catch(this.handleError);
	}

	markOneAsRead(notificationTag): Promise<any> {
		return this.backEndService
	        .sendReadNotifications([notificationTag]) 
	        .then(res => {  	           	
	        	var found = this.newNotifications.find(function(notification) {
					  return notification.tag == notificationTag;
					}, notificationTag);
				if(found != undefined) {
					var index = this.newNotifications.indexOf(found);
					this.oldNotifications.unshift(found);
					if (index > -1) {
					 	this.newNotifications.splice(index, 1);
					}
				}

	            if(this.counter > 0) {
	            	this.counter--;
	            	this.setIconBadge();
	            }	           
	         })  
          	.catch(this.handleError);
	}

	clearNewNotifications() {
		for(var i=this.readNotifications.length; i > 0; i--) {
			this.oldNotifications.unshift(this.readNotifications[(i-1)]);
		}

		this.readNotifications = [];
	}

	private setIconBadge() {
		this.badge.set(this.counter);
	}

	private handleError(error: any) {
		return Promise.reject(error.message || error);
	}

  
}