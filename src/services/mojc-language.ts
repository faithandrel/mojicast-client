import { Injectable } from '@angular/core';

import { MojcStorage } from './mojc-storage';

import { 
  ENGLISH,
  FILIPINO,
  LANGUAGE_TEXT
} from '../constants';

@Injectable()
export class MojcLanguage {

  private savedLanguage: string = ENGLISH;
  private languageTextSet: object = LANGUAGE_TEXT;

  constructor(private storage: MojcStorage) {
    
  }

  saveLanguage(language): Promise<any> {
    return this.storage.setLanguage(language)
      .then((res) => {
        this.savedLanguage = language;
        return Promise.resolve(this.savedLanguage);
       })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  loadSavedLanguage(): Promise<any> {
    return this.storage.getLanguage()
       .then((res) => {
        this.savedLanguage = res ? res : ENGLISH;
        return Promise.resolve(this.savedLanguage);
       })
      .catch(e => {
        return Promise.reject(e);
      });
  }
  
  getTextFor(textName) {
    return this.languageTextSet[this.savedLanguage].text[textName];
  }

}