import { Injectable } from '@angular/core';

import { MojcStorage } from './mojc-storage';

import { 
  WRITE_POST, 
  SET_LANGUAGE 
} from '../constants';

@Injectable()
export class PromptHandler {
  
  public savedPrompts: Array<string> = [];

  constructor(private storage: MojcStorage,) {
    
  }
  
  isDoneFor(promptName): boolean {
    return this.savedPrompts.indexOf(promptName) != -1;
  }

  save(promptName): Promise<any> {
    if(!this.isDoneFor(promptName)) {
      this.savedPrompts.push(promptName);
    }
    return this.storage.savePrompts(this.savedPrompts);
  }

  getCompleted(): Promise<any>{
     return this.storage.getPrompts()
     .then((res) => {
        this.savedPrompts = res ? res : [];
        return Promise.resolve(this.savedPrompts);
       })
      .catch(e => {
        return Promise.reject(e);
      });
  }

}