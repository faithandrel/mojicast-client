import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

import { Subject } from 'rxjs/Subject';

@Injectable()
export class MojcErrorHandler {
  onUnauthorizedRequest: any;

  constructor(public toastCtrl: ToastController) {
    this.onUnauthorizedRequest = new Subject();
  }
  
  handle(theError: any) {
  	var errorType = '';

  	try {
    	errorType = theError.constructor.name;
    }
    catch (e) {
      
    };
    
    if(errorType == 'MojcError' && theError.show) {
      let toast = this.toastCtrl.create({
        message: theError.message,
        duration: 3000,
      });
      
      toast.present();
    }

    if(errorType == 'Response' && theError.status == 401) {
      this.onUnauthorizedRequest.next(theError);
    }

    //TODO: add logging for other errors

  }

}