import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { Headers, Http, Response, Request, RequestOptions, RequestMethod } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { AuthHttp, AuthConfig, tokenNotExpired, JwtHelper } from 'angular2-jwt';
import 'rxjs/add/operator/toPromise';
import { MojcStorage } from './mojc-storage';
import { BACKEND_SERVER } from '../constants';

@Injectable()
export class BackEndService {
  backEndUrl = BACKEND_SERVER;  // URL to web api
  jwtHelper: JwtHelper = new JwtHelper();
  
  constructor(private http: Http,
              private storage: MojcStorage,
              private network: Network,
              public toastCtrl: ToastController) {
  }

  checkWeb() {
    if (this.network.type == 'none') {
      let toast = this.toastCtrl.create({
        message: 'Please connect to the internet.',
        duration: 5000,
      });
      
      toast.present();
    } 
  }
  
  //TODO: probably move to own service, make return a Promise
  private prepareHttpRequest(values, url: string, withAuth: boolean, method) {
    if(values == null) {
      values = {};
    }
    values._refresh = this.storage.sendJwtRefresh;
    let body = JSON.stringify(values);

    let requestUrl = this.backEndUrl + url;
    if(method == RequestMethod.Get && this.storage.sendJwtRefresh == true) {
      requestUrl += '?_refresh=1';
    }
    
    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    this.checkWeb();

    if(withAuth === true) {
       var auth = 'Bearer ' + this.storage.jwtToken;
       myHeaders.append('Authorization', auth);
    }

    var options = new RequestOptions({
      method: method,
      url: requestUrl,
      headers: myHeaders,
      body: body
    });
    return new Request(options);
  }

  private processHttpResponse(res) {
    if(this.storage.sendJwtRefresh == true) {
      var token  = this.parseToken(res.headers);
      if (token) {
        this.authSuccess(token);
        this.storage.sendJwtRefresh = false;
      }
    }
    return res.json();
  }

  saveUsername(signUpUser): Promise<any> {
    var req = this.prepareHttpRequest(signUpUser, 'save-username', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }
  
  getItems(location, page: number): Promise<any> {
    let body = {location: location};
    var req = this.prepareHttpRequest(body, 'get-items?page='+page, true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }
  
  saveItem(item): Promise<any> {
    var req = this.prepareHttpRequest(item, 'save-item', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getSingleItem(itemId: number): Promise<any> {
    var req = this.prepareHttpRequest(null, 'item/'+ itemId, true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }
  
  getItemComments(itemId: number, page: number): Promise<any> {
    let body = {item: itemId};
    var req = this.prepareHttpRequest(body, 'get-comments?page='+page, true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getUserItems(page: number): Promise<any> {
    var req = this.prepareHttpRequest(null, 'user-items?page='+page, true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  private parseToken(headers): string {
    var authHeader = headers.get('Authorization');
    if(authHeader == null) {
      return null;
    }
    var header = authHeader.split(" ");
    return header[1];
  }
  
  authSuccess(token): Promise<any> {
    var payload = this.jwtHelper.decodeToken(token);
    return this.storage.setJwt(token, payload);
  }
  
  getSavedJwt() {
    //TODO: move jwt function to own service
    return this.storage.getSavedJwt().then(token => {
      return token;
    });
  }
  
  loginWithFacebook(userObject) {
    let body = userObject;
    
    var req = this.prepareHttpRequest(body, 'facebook-log-in', false, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => {
            var token  = this.parseToken(res.headers);
            let data   = res.json();

            if (token != undefined ) {
              return this.authSuccess(token);
            }
            else {
              return Promise.reject('Log in failed.');
            }
          })
          .catch(this.handleError);
  }

  getExploreFeed(page: number): Promise<any> {
    var req = this.prepareHttpRequest(null, 'explore-feed?page='+page, true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getEmojiFeed(emoji, location, page: number): Promise<any> {
    let body = {emoji: emoji, location: location};
    var req = this.prepareHttpRequest(body, 'emoji?page='+page, true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getNewNotifications(): Promise<any> {
    var req = this.prepareHttpRequest(null, 'new-notifications', true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  getAllNotifications(): Promise<any> {
    var req = this.prepareHttpRequest(null, 'all-notifications', true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getOldNotifications(readNotifs: Array<string>, page: number): Promise<any> {
    let body = {ids: readNotifs};
    var req = this.prepareHttpRequest(body, 'old-notifications?page='+page, true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  sendReadNotifications(notificationIds: Array<string>): Promise<any> {
    let body = {ids: notificationIds};
    var req = this.prepareHttpRequest(body, 'notifications', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  sendPendingLocation(itemIds: Array<number>, latitude: number, longitude: number): Promise<any> {
    let body = {
                  items: itemIds,
                  latitude: latitude,
                  longitude: longitude
                };
    var req = this.prepareHttpRequest(body, 'item-locations', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  getItemsWithoutLocation(): Promise<any> {
    var req = this.prepareHttpRequest(null, 'no-location-items', true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  logout(): Promise<any> {
    var req = this.prepareHttpRequest(null, 'logout', true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  openChatbox(user2Id): Promise<any> {
    let body = {user2: user2Id};
    var req = this.prepareHttpRequest(body, 'open-chatbox', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  requestChatbox(user2Id, message): Promise<any> {
    let optionalMsg = message ? message : ''; 
    let body = {user2: user2Id, description: optionalMsg};
    
    var req = this.prepareHttpRequest(body, 'request-chatbox', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  confirmChatbox(chatboxId): Promise<any> {
    let body = {chatbox: chatboxId};
    var req = this.prepareHttpRequest(body, 'confirm-chatbox', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  declineChatbox(chatboxId): Promise<any> {
    let body = {chatbox: chatboxId};
    var req = this.prepareHttpRequest(body, 'decline-chatbox', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getChatboxDetails(chatboxId): Promise<any> {
    let body = {chatbox: chatboxId};
    var req = this.prepareHttpRequest(body, 'chatbox-details', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  checkChatboxStatus(user2Id): Promise<any> {
    let body = {user2: user2Id};
    var req = this.prepareHttpRequest(body, 'check-chatbox', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getChatboxes(page: number): Promise<any> {
    var req = this.prepareHttpRequest(null, 'get-chatboxes?page='+page, true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getChatSettings(): Promise<any> {
    var req = this.prepareHttpRequest(null, 'get-chat-settings', true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  saveChatSettings(data): Promise<any> {
    var req = this.prepareHttpRequest(data, 'save-chat-settings', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getUserBio(userId): Promise<any> {
    let body = {user: userId};
    var req = this.prepareHttpRequest(body, 'user-bio', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getMatch(matchId: number): Promise<any> {
    var req = this.prepareHttpRequest(null, 'match/'+ matchId, true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  sendMatchResponse(matchId, accepted: boolean): Promise<any> {
    let body = {match: matchId, accepted: accepted};
    var req = this.prepareHttpRequest(body, 'respond-match', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  getActiveUsers(page: number): Promise<any> {
    var req = this.prepareHttpRequest(null, 'active-users?page='+page, true, RequestMethod.Get);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  openChatFromMatch(user2Id): Promise<any> {
    let body = {user2: user2Id};
    var req = this.prepareHttpRequest(body, 'chat-from-match', true, RequestMethod.Post);
    
    return this.http.request(req)
          .toPromise()
          .then(res => this.processHttpResponse(res))
          .catch(this.handleError);
  }

  private handleError(error: any) {
    return Promise.reject(error.message || error);
  }
}