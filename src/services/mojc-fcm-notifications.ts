import { ItemPage } from '../pages/item/item';
import { ChatboxPage } from '../pages/chatbox/chatbox';
import { ConfirmChatboxComponent } from '../components/confirm-chatbox/confirm-chatbox';
import { OpenMatchComponent } from '../components/open-match/open-match';

import { 
	PENDING_CHATBOX_STATUS,
	ACTIVE_CHATBOX_STATUS,
	DECLINED_CHATBOX_STATUS,
	PENDING_MATCH_STATUS
} from '../constants';

export class ItemFCMNotificaton {
	public navCtrl: any;
	public item: any;

	constructor(item: any, navCtrl: any) {
		this.item = item;
		this.navCtrl = navCtrl;
	}

	public openFCMNotification() {
		this.navCtrl.push(ItemPage, { item: this.item });
	}
}

export class ChatRequestNotification {
	public navCtrl: any;
	public modalCtrl: any;
	public messagingService: any;
	public details: any;

	constructor(details: any, navCtrl: any, modalCtrl: any, messagingService: any) {
		this.details = details;
		this.navCtrl = navCtrl;
		this.modalCtrl = modalCtrl;
		this.messagingService = messagingService;
	}

	public openFCMNotification() {
		if(this.details.status == PENDING_CHATBOX_STATUS) {
        	let modal = this.modalCtrl.create(ConfirmChatboxComponent, {
        													details: this.details, 
        													chatbox: this.details.chatboxId});
        	modal.present();
		}
		if(this.details.status == ACTIVE_CHATBOX_STATUS) {
			this.navCtrl.push(ChatboxPage, { user: this.details.user_id });
		}
		if(this.details.status == DECLINED_CHATBOX_STATUS) {
			this.messagingService.showDeclinedAlert();
		}
	}
}

export class NewMatchNotification {
	public matchingService: any;
	public modalCtrl: any;
	public match: any;
	public user: any;

	constructor(matchingService: any, modalCtrl: any, match: any, user: any) {
		this.matchingService = matchingService;
		this.modalCtrl = modalCtrl;
		this.match = match;
		this.user = user;
	}

	public openFCMNotification() {
		if(this.match.status != PENDING_MATCH_STATUS) {
	    		this.matchingService.showOpenedAlert();
    	} 
    	else {
  			let modal = this.modalCtrl.create(OpenMatchComponent, {match: this.match.id, user: this.user});
	        modal.present();
	    }
	}
}