import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { MojcNotification } from './mojc-notification';
import { MojcLocation } from './mojc-location';
import { MojcStorage } from './mojc-storage';
import { ItemService } from './item-service';
import { MatchingService } from './matching-service';
import { BackEndService } from './back-end-service';
import { MojcErrorHandler } from './mojc-error-handler';

import { MojcError } from './mojc-error';

@Injectable()
export class MojcConfiguration {

	checkInterval: number = 60000; //milliseconds
	notifIntervalCheck: any;

	constructor(private notification: MojcNotification,
				private location: MojcLocation,
				private storage: MojcStorage,
				private itemService: ItemService,
				private matchingService: MatchingService,
				private mojcErrorHandler: MojcErrorHandler) {

	}

	runChecks() {
		this.notification.getNewNotifications()
		.catch((error) => {
			var theError = new MojcError(error, 'Unauthorized request.', false);

			this.mojcErrorHandler.handle(theError);		
		});

		this.storage.checkTokenExpiry();

		this.location.monitorGeo().then((res) => {
			this.itemService.saveItemLocation(res.latitude, res.longitude);
		})
		.catch((error) => {
			this.mojcErrorHandler.handle(error);
		}); 

		this.matchingService.checkUsersList()
		.catch((error) => {
			var theError = new MojcError(error, 'Unauthorized request.', false);

			this.mojcErrorHandler.handle(theError);		
		});
	}

	runChecksOnInterval() {		
	    var interval = Observable.interval(this.checkInterval);
	    
	    this.notifIntervalCheck = interval.subscribe(interval => {
			this.runChecks();
	    },
	    	err => {  }
	    );
  	}

  	turnOffIntervalChecks() {
  		this.notifIntervalCheck.unsubscribe();
  	}

	private handleError(error: any) {
		return Promise.reject(error.message || error);
	}
}