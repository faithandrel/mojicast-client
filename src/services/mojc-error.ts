export class MojcError {
  public show: boolean = false;
  public message: string;
  public theError: any;

  constructor(theError: any, message: string, show: boolean) {
  	this.theError = theError;
  	this.show     = show;
  	this.message  = message;
  }
}