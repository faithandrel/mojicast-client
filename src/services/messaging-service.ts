import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AlertController } from 'ionic-angular';

import { BackEndService } from './back-end-service';

@Injectable()
export class MessagingService {
  counter: number = 0;
  chatboxes: Array<any> = [];
  currentPage: number = 0;
  total: number = 0;
  checkInterval: number = 40000; //milliseconds
  messageIntervalCheck: any;

  constructor(public alertCtrl: AlertController, 
              private backEndService: BackEndService) {
    
  }
  
  getChatboxes(nextPage: boolean): Promise<any>{
      var page = 0;
      if(nextPage) {
        page = this.currentPage + 1;
      }

      return this.backEndService.getChatboxes(page)
            .then(res => { 
              this.counter = res.unread_count;
              
              if(page == 0) {
                this.chatboxes = [];
              }
              for (let i = 0; i < res.chatboxes.length; i++) {
                this.chatboxes.push(res.chatboxes[i]);
              }

              this.currentPage = res.current_page;
              this.total = res.total
              
              return {
                chatboxes: this.chatboxes,
                total: res.total
              };
            })          
            .catch(this.handleError);
  }

  decrementCounter(value: number) {
      if(this.counter > 0) {
        this.counter -= value;

        if(this.counter < 0) {
          this.counter = 0;
        }
      }
  }

  getMessagesOnInterval() {
      var interval = Observable.interval(this.checkInterval);
        
      this.messageIntervalCheck = interval.subscribe(interval => {
        this.getChatboxes(false);
      },
        err => {  }
      );
  }

  turnOffMessageChecks() {
    this.messageIntervalCheck.unsubscribe();
  }

  getChatSettings(): Promise<any> {
    return this.backEndService.getChatSettings();
  }

  getChatboxDetails(chatboxId): Promise<any> {
    return this.backEndService.getChatboxDetails(chatboxId);
  }

  confirmRequest(chatboxId): Promise<any> {
    return this.backEndService.confirmChatbox(chatboxId);
  }

  declineRequest(chatboxId): Promise<any> {
    return this.backEndService.declineChatbox(chatboxId);
  }

  showDeclinedAlert() {
    let alert = this.alertCtrl.create({
                 subTitle: 'You have declined to chat with this user.',          
                 buttons: [{
                            text: 'Ok',
                            handler: () => {
                             
                            }
                           }]
               });
    alert.present();
  }

  private handleError(error: any) {
      return Promise.reject(error.message || error);
  }

  /*private setIconBadge() {
    this.badge.set(this.counter);
  }*/

}