import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

@Injectable()
export class MojcFacebook {

  constructor(private fb: Facebook) {
  }
  
  loginWithFacebook(): Promise<any> {
    return this.fb.login(['email'])
      .then((res: FacebookLoginResponse) => {
        return Promise.resolve(res);
       })
      .catch(e => {
      	return Promise.reject(e);
      });

  }
  
}