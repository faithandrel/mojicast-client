import { Injectable } from '@angular/core';
import { ToastController, AlertController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';
import { MojcErrorHandler } from './mojc-error-handler';
import { MojcError } from './mojc-error';
import 'rxjs/Rx';

@Injectable()
export class MojcLocation {
  
  locationStatus: any = false;
  public myLocation: any = false;
  geoStream: any = false;
  latitude: number = 0;
  longitude: number = 0;
  timestamp: number = 0;
  lastError: any = null;

  constructor(public toastCtrl: ToastController, 
              public alertCtrl: AlertController,
              private mojcErrorHandler: MojcErrorHandler,
              private geolocation: Geolocation,
              private diagnostic: Diagnostic) {
     
  }
  
  monitorGeo() {
    var options = {maximumAge: 20000, timeout: 3600, enableHighAccuracy: true};

    return this.geolocation.getCurrentPosition(options).then((res) => {

      //return fresh location
      if(res.coords.latitude > 0 && res.coords.longitude > 0) {
        this.latitude  = res.coords.latitude;
        this.longitude = res.coords.longitude;
        this.timestamp = res.timestamp;

        return Promise.resolve({ 
                latitude: this.latitude,
                longitude: this.longitude
              });
      }
    }).catch((error) => {
      this.lastError = {
        code: error.code,
        message: error.message
      }

      return Promise.reject(error);
    });
  }
  
  getGeo(): Promise<any> {
    this.checkGeo()
    .then(res => {
        
     })          
    .catch(error => {
        this.mojcErrorHandler.handle(error);
    })

    var options = {maximumAge: 20000, timeout: 3600, enableHighAccuracy: true};

    return this.geolocation.getCurrentPosition(options).then((res) => {
      //return fresh location
      if(res.coords.latitude !== 0 && res.coords.longitude !== 0) {
        this.latitude  = res.coords.latitude;
        this.longitude = res.coords.longitude;
        this.timestamp = res.timestamp;
        return Promise.resolve({ 
                latitude: this.latitude,
                longitude: this.longitude
              });
      }
      //return cached location
      //TODO: time limit
      if(this.latitude !== 0 && this.longitude !== 0) {
         return Promise.resolve({ 
                latitude: this.latitude,
                longitude: this.longitude
              });
      }

      this.lastError = {
        code: 4,
        message: 'No valid location.'
      }

      return Promise.reject(res);
    }).catch((error) => {
      this.lastError = {
        code: error.code,
        message: error.message
      }

      //return cached location
      if(this.latitude !== 0 && this.longitude !== 0) {
         return Promise.resolve({ 
                latitude: this.latitude,
                longitude: this.longitude
              });
      }

      return Promise.reject(error);
    });
  }

  checkGeo(): Promise<any> {
    return this.diagnostic.isLocationEnabled().then(res => {
      if (!res) {
        var error = new MojcError(res, 'Please turn on your location.', true);
        return Promise.reject(error);
      }
      return Promise.resolve(res);
    })
    .catch(e => {return Promise.reject(e)});
  }

  showLocationAlert() {
    let alert = this.alertCtrl.create({
           title: 'Please turn on location!',
           subTitle:  'So we can find relevant content for you. Thanks!',
           buttons: [{
                      text: 'Ok',
                      handler: () => {
                        this.switchToLocationSettings();
                      }
                    }]
         });
    alert.present();
  }

  checkGeoPermission() {
    return this.diagnostic.isLocationAuthorized();
  }

  switchToLocationSettings() {
    return this.diagnostic.switchToLocationSettings();
  }

  requestLocationAuthorization() {
    return this.diagnostic.requestLocationAuthorization();
  }
}