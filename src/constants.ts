//ENDPOINTS
export const BACKEND_SERVER = '';
export const CHAT_SERVER = '';

//First install prompts
export const WRITE_POST = 'write-post';
export const CHANGE_ALIAS = 'change-alias';
export const EXPLORE_EMOJI = 'explore-emoji';
export const COMMENT_ALIAS = 'comment-alias';
export const SET_LANGUAGE = 'set-language';
export const SET_MESSAGING = 'set-messaging';

//Language
export const ENGLISH  = 'en';
export const FILIPINO = 'fil';
export const ENGLISH_TEXT  = 'English';
export const FILIPINO_TEXT = 'Filipino';

export const LANGUAGE_TEXT = {
	'en': 
	{
		name: 'English',
		text:  
		{
			'write-post': 'This is where you write your first post!',
			'change-alias': 'You can change the your pen name on every post.',
			'explore-emoji': 'Explore posts with emojis!',
			'comment-alias': 'You can also change your name on any discussion.',
			'new-chatbox': 'Create new chatbox.',
			'notified-chatbox': 'We have notified the user'
		}
	},
	'fil': 
	{
		name: 'Filipino',
		text: 
		{
			'write-post': 'Dito mo isusulat ang iyong unang kwento!',
			'change-alias': 'Maari mong ibahin ang iyong username sa bawat kwento.',
			'explore-emoji': 'Mag hanap ng mga kwento gamit ang mga emojis.',
			'comment-alias': 'Maari mo ring ibahin ang iyong username sa mga komento.',
			'new-chatbox': 'Gumawa nang bagong chatbox.',
			'notified-chatbox': 'Aming pinag-alam ang user.'
		}
	}
};

//Chat prompts
export const NEW_CHATBOX = {
	status:    'new',
	text_code: 'new-chatbox'
};
export const ACTIVE_CHATBOX = {
	status:    'active',
	text_code: 'active-chatbox'
};
export const ACTIVATED_CHATBOX = {
	status:    'activated',
	text_code: 'activated-chatbox'
};
export const NOTIFIED_CHATBOX = {
	status:    'notified',
	text_code: 'notified-chatbox'
};
export const DECLINED_CHATBOX = {
	status:    'declined',
	text_code: 'declined-chatbox'
};

//Chatbox status
export const PENDING_CHATBOX_STATUS = 0;
export const ACTIVE_CHATBOX_STATUS = 1;
export const DECLINED_CHATBOX_STATUS = 2;

export const CREATED_CHATBOX = 'created-chatbox';

//Notification types
export const NEW_COMMENT = {
	type: 'new_comment'
};
export const VIEW_COUNT = {
	type: 'view_count'
};
export const CHAT_REQUEST = {
	type: 'chat_request'
};
export const NEW_MATCH = {
	type: 'new_match'
};
export const NEW_MESSAGE = {
	type: 'new_message'
};
export const CONFIRMED_CHAT = {
	type: 'confirmed_chat'
};

//Match status
export const PENDING_MATCH_STATUS = 0;
export const USER_ACCEPT_MATCH_STATUS = 1;
export const USER_DECLINED_MATCH_STATUS = 2;
export const USER2_ACCEPT_MATCH_STATUS = 3;
export const USER2_DECLINED_MATCH_STATUS = 4;

