import { ViewController, LoadingController, NavParams, AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatchingService } from '../../services/matching-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';


@Component({
  selector: 'open-match',
  templateUrl: 'open-match.html'
})
export class OpenMatchComponent {

  matchId: number;
  bio: string = '';
  username: string = '';

  constructor(public viewCtrl: ViewController, 
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private navParams: NavParams,         
              private matchingService: MatchingService,
              private mojcErrorHandler: MojcErrorHandler,
               ) {
      
      let user = navParams.get('user');
      this.matchId = navParams.get('match');
      this.bio = user.bio;
      this.username = user.name;    
  }

  accept() {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    
    this.matchingService.accept(this.matchId)
    .then(res => {    
        loader.dismiss();
        this.showAcceptAlert();
     })
    .catch(error => {
        loader.dismiss();
        this.mojcErrorHandler.handle(error);
    });
  }

  decline() {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    
    this.matchingService.decline(this.matchId)
    .then(res => {    
        loader.dismiss();
        this.showDeclineAlert();
     })
    .catch(error => {
        loader.dismiss();
        this.mojcErrorHandler.handle(error);
    });
  }

  closeModal() {
      this.viewCtrl.dismiss();
  }

  showAcceptAlert() {
    let alert = this.alertCtrl.create({
                 subTitle: 'Done! Your match will respond soon.',          
                 buttons: [{
                            text: 'Ok',
                            handler: () => {
                              this.closeModal();
                            }
                           }]
               });
    alert.present();
  }

  showDeclineAlert() {
    let alert = this.alertCtrl.create({
                 subTitle: 'No worries! We will send you another match soon.',          
                 buttons: [{
                            text: 'Ok',
                            handler: () => {
                              this.closeModal();
                            }
                           }]
               });
    alert.present();
  }

}
