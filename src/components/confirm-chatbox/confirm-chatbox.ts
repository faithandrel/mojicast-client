import { ViewController, LoadingController, NavParams, NavController, AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { MessagingService } from '../../services/messaging-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { ChatboxPage } from '../../pages/chatbox/chatbox';

import { DECLINED_CHATBOX } from '../../constants';


@Component({
  selector: 'confirm-chatbox',
  templateUrl: 'confirm-chatbox.html'
})
export class ConfirmChatboxComponent {

  details: any;
  chatboxId: number;
  fromMatch: boolean;

  constructor(public viewCtrl: ViewController, 
              public loadingCtrl: LoadingController,
              public navCtrl: NavController,
              public alertCtrl: AlertController,
              private navParams: NavParams,
              private messagingService: MessagingService,
              private mojcErrorHandler: MojcErrorHandler,
               ) {
      this.details  = navParams.get('details');
      this.chatboxId = navParams.get('chatbox');   
      this.fromMatch = navParams.get('from_match') != undefined ? navParams.get('from_match') : false;
  }

  confirm() {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    
    this.messagingService.confirmRequest(this.chatboxId)
    .then(res => {    
        loader.dismiss();
        this.navCtrl.push(ChatboxPage, { user: res.user }); 
        this.closeModal();
     })
    .catch(error => {
        loader.dismiss();
        this.mojcErrorHandler.handle(error);
    });
  }

  decline() {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    
    this.messagingService.declineRequest(this.chatboxId)
    .then(res => {    
        loader.dismiss();
        this.showDeclinedAlert();
     })
    .catch(error => {
        loader.dismiss();
        this.mojcErrorHandler.handle(error);
    });
  }

  showDeclinedAlert() {
    let alert = this.alertCtrl.create({
                 subTitle: 'You have declined to chat with this user.',          
                 buttons: [{
                            text: 'Ok',
                            handler: () => {
                              this.closeModal();
                            }
                           }]
               });
    alert.present();
  }

  showMessage() {
    return !this.fromMatch && this.details.message.length > 0;
  }

  closeModal() {
      this.viewCtrl.dismiss();
  }

}
