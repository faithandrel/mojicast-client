import { ViewController, LoadingController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BackEndService } from '../../services/back-end-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';


@Component({
  selector: 'message-settings',
  templateUrl: 'message-settings.html'
})
export class MessageSettingsComponent {

  msgSettings: any;
  bioExample: string;
  maxLength: number = 280;
  
  constructor(public viewCtrl: ViewController, 
              public loadingCtrl: LoadingController,
              private navParams: NavParams,
              private formBuilder: FormBuilder,
              private backEndService: BackEndService,
              private mojcErrorHandler: MojcErrorHandler,
               ) {
      let savedSettings = navParams.get('savedSettings');
      
      this.msgSettings = this.formBuilder.group({
        userBio: [savedSettings.bio, [Validators.required, Validators.maxLength(this.maxLength)]],
        sendChatRequest: [savedSettings.send_requests],
        allowFromPosts: [savedSettings.allow_from_posts]
      });

      this.bioExample = "Tell us about yourself and who you want to meet! \n\n" + 
                        "Ex. 'I'm a low-key romantic who wants to meet someone cute, kind, and most importantly, with a great sense of humor.'\n\n" +
                        "Note: This will show up on your matches!";
  }

  saveForm() {
      let loader = this.loadingCtrl.create({content: 'Loading...'});
      loader.present()

      var chatSettings = {
        bio: this.msgSettings.get('userBio').value,
        send_requests: this.msgSettings.get('sendChatRequest').value,
        allow_from_posts: this.msgSettings.get('allowFromPosts').value,
      };

      this.backEndService.saveChatSettings(chatSettings)
      .then(res => {
        this.closeModal();
        loader.dismiss();
      })
      .catch(error => {
        loader.dismiss();
        this.mojcErrorHandler.handle(error);
      });
  }

  closeModal() {
      this.viewCtrl.dismiss();
  }

  characterCount() {
    return this.msgSettings.get('userBio').value.length;
  }

  counterClass() {
    if(this.characterCount() > this.maxLength) {
      return 'character-count invalid';
    }
    return 'character-count';
  }

}
