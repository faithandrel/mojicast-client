import { ViewController, LoadingController, NavParams, AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BackEndService } from '../../services/back-end-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';


@Component({
  selector: 'user-bio',
  templateUrl: 'user-bio.html'
})
export class UserBioComponent {

  requestChat: any;
  bio: string = '';
  username: string = '';
  userId: number;

  constructor(public viewCtrl: ViewController, 
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private navParams: NavParams,
              private formBuilder: FormBuilder,
              private backEndService: BackEndService,
              private mojcErrorHandler: MojcErrorHandler,
               ) {
      this.userId  = navParams.get('userId');
      this.username = navParams.get('username');
      this.getUserBio(this.userId);
      this.requestChat = this.formBuilder.group({
        message: ['', Validators.maxLength(280)]
      });
  }

  requestChatbox() {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    
    var message = this.requestChat.get('message').value;

    this.backEndService
        .requestChatbox(this.userId, message)
        .then(res => {
            loader.dismiss();
            if(res) {
              let alert = this.alertCtrl.create({
                   subTitle: 'We sent your request!',          
                   buttons: [{
                              text: 'Ok',
                              handler: () => {
                                this.closeModal();
                              }
                             }]
                 });
               alert.present();
            }
        })
        .catch(error => {
            loader.dismiss();
            this.mojcErrorHandler.handle(error);
        });
  }

  getUserBio(userId) {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    
    this.backEndService.getUserBio(userId)
    .then(res => {
        this.bio = res.bio;
        loader.dismiss();
     })
    .catch(error => {
        loader.dismiss();
        this.mojcErrorHandler.handle(error);
    });
  }

  closeModal() {
      this.viewCtrl.dismiss();
  }

}
