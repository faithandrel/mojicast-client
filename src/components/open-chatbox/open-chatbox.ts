import { LoadingController, NavController, AlertController, ModalController } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { BackEndService } from '../../services/back-end-service';
import { MojcLanguage } from '../../services/mojc-language';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { ChatboxPage } from '../../pages/chatbox/chatbox';
import { UserBioComponent } from '../user-bio/user-bio';

import { 
  NEW_CHATBOX, 
  ACTIVE_CHATBOX, 
  ACTIVATED_CHATBOX,
  NOTIFIED_CHATBOX,
  DECLINED_CHATBOX
} from '../../constants';

@Component({
  selector: 'open-chatbox',
  templateUrl: 'open-chatbox.html'
})
export class OpenChatboxComponent {

  @Input() toUser: number;
  @Input() username: string;

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              private backEndService: BackEndService,
              private mojcLang: MojcLanguage,
              private mojcErrorHandler: MojcErrorHandler) {
  }

  newChatboxPrompt() {
    let modal = this.modalCtrl.create(UserBioComponent, {userId: this.toUser, username: this.username});
    modal.present();
  }

  notifiedChatboxPrompt() {
     let alert = this.alertCtrl.create({
         subTitle: this.mojcLang.getTextFor(NOTIFIED_CHATBOX.text_code),          
         buttons: [{
                    text: 'Ok',
                    handler: () => {
                        
                    }
                   }]
       });
    alert.present();
  }

  declinedChatboxPrompt() {
    let alert = this.alertCtrl.create({
         subTitle: 'Declined',          
         buttons: [{
                    text: 'Ok',
                    handler: () => {
                        
                    }
                   }]
       });
    alert.present();
  }

  checkChatbox() {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    this.backEndService
        .checkChatboxStatus(this.toUser)
        .then(res => {
            loader.dismiss();
            if(res.status == NEW_CHATBOX.status) {
                this.newChatboxPrompt();
            }
            else if(res.status == NOTIFIED_CHATBOX.status) {
                this.notifiedChatboxPrompt();
            }
            else if(res.status == ACTIVE_CHATBOX.status || res.status == ACTIVATED_CHATBOX.status) {
                this.navCtrl.push(ChatboxPage, { user: this.toUser });
            }
            else if(res.status == DECLINED_CHATBOX.status) {
                 this.declinedChatboxPrompt();   
            }

        })
        .catch(error => {
            loader.dismiss();
            this.mojcErrorHandler.handle(error);
        });
  }

}
