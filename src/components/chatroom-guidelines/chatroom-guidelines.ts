import { ViewController } from 'ionic-angular';
import { Component } from '@angular/core';
import { MojcErrorHandler } from '../../services/mojc-error-handler';


@Component({
  selector: 'chatroom-guidelines',
  templateUrl: 'chatroom-guidelines.html'
})
export class ChatroomGuidelinesComponent {

  constructor(public viewCtrl: ViewController, 
              private mojcErrorHandler: MojcErrorHandler,
               ) {   
  }

  closeModal() {
      this.viewCtrl.dismiss();
  }

}
