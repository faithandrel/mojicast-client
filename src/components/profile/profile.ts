import { Component, ViewChild } from '@angular/core';
import { App, ViewController } from 'ionic-angular';
import { BackEndService } from '../../services/back-end-service';
import { MojcStorage } from '../../services/mojc-storage';
import { MojcConfiguration } from '../../services/mojc-configuration';
import { MessagingService } from '../../services/messaging-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { LogInPage } from '../../pages/log-in/log-in';
import { CategoryPage } from '../../pages/category/category';

@Component({
  selector: 'profile',
  templateUrl: 'profile.html'
})

export class ProfileComponent {

	userName: any;
  
  constructor(public viewCtrl: ViewController,
              public appCtrl: App,
              private backEndService: BackEndService,
  			      private storage: MojcStorage,
              private mojcConfig: MojcConfiguration,
              private messaging: MessagingService,
  			      private mojcErrorHandler: MojcErrorHandler) {
  		this.storage.getSavedUsername()
                .then(res => {  
                    this.userName = res;
                  })          
                .catch(error => {
                    this.mojcErrorHandler.handle(error);
                });
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  logout() {
    this.backEndService.logout().then(res => {  
                  return this.storage.clearUser();
                })     
                .then(res => {  
                    this.mojcConfig.turnOffIntervalChecks();
                    this.messaging.turnOffMessageChecks();
                    this.closeModal();
                    this.appCtrl.getRootNav().setRoot(LogInPage);
                  })          
                .catch(error => {
                    this.mojcErrorHandler.handle(error);
                });
  }

}
