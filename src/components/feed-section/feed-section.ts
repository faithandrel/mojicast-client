import { LoadingController, NavController } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { ItemService } from '../../services/item-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { ItemPage } from '../../pages/item/item';

@Component({
  selector: 'feed-section',
  templateUrl: 'feed-section.html'
})
export class FeedComponent {

  @Input() itemList:  Array<any>;
  @Input() postedItems: Array<any>;

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              private itemService: ItemService,
              private mojcErrorHandler: MojcErrorHandler) {
  }

  viewSingleItem(itemId) {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    this.itemService
        .getSingleItem(itemId)
        .then(res => {
            this.navCtrl.push(ItemPage, { item: res });
            loader.dismiss();
        })
        .catch(error => {
            loader.dismiss();
            this.mojcErrorHandler.handle(error);
        });
  }

}
