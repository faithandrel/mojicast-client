import { ViewController, LoadingController, AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ItemService } from '../../services/item-service';
import { MojcErrorHandler } from '../../services/mojc-error-handler';
import { MojcLocation } from '../../services/mojc-location';
import { MojcStorage } from '../../services/mojc-storage';
import { MojcLanguage } from '../../services/mojc-language';
import { PromptHandler } from '../../services/prompt-handler';

import { MojcError } from '../../services/mojc-error';

import { CHANGE_ALIAS } from '../../constants';

@Component({
  selector: 'add-item',
  templateUrl: 'add-item.html'
})
export class AddItemComponent {

  article: any;
  newItem: any;
  userName: any;
  hideNameField: boolean = true;
  
  constructor(public viewCtrl: ViewController, 
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private formBuilder: FormBuilder,
              private itemService: ItemService,
              private mojcErrorHandler: MojcErrorHandler,
              private mojcLocation: MojcLocation,
              private storage: MojcStorage,
              private mojcLang: MojcLanguage,
              private promptHandler: PromptHandler
               ) {

    this.article = this.formBuilder.group({
      name: [''],
      title: ['', Validators.required],
      contentBody: ['', Validators.required],
      recurring: ['false']
    });

    this.storage.getSavedUsername()
                .then(res => {  
                    this.userName = res;
                  })          
                .catch(error => {
                    this.mojcErrorHandler.handle(error);
                });
  }

  saveForm() {
    let loader = this.loadingCtrl.create({content: 'Loading...'});
    loader.present()
    
    var item     = {};
    this.newItem = {
      title: this.article.get('title').value,
      content: this.article.get('contentBody').value,
      name: this.article.get('name').value,
    };

    this.itemService.saveItem(this.newItem)
    .then(res => {
      this.closeModal();
      loader.dismiss();
    })
    .catch(error => {
      loader.dismiss();
      if(error.status != undefined && error.status == 422) {
        var contents = error.json();
        if(contents.errors.content != undefined && contents.errors.content.length > 0) {
          error = new MojcError(error, contents.errors.content[0], true);
        }
        if(contents.errors.name != undefined && contents.errors.name.length > 0) {
          error = new MojcError(error, contents.errors.name[0], true);
        }
      } //TODO: move to service
      this.mojcErrorHandler.handle(error);
    });
 
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  toggleNameField() {
    if(this.hideNameField) {
      this.hideNameField = false;
    }
    else {
      this.hideNameField = true;
    }
    if(this.showChangeAliasPrompt()) {
      let alert = this.alertCtrl.create({
           subTitle: this.mojcLang.getTextFor(CHANGE_ALIAS),          
           buttons: [{
                      text: 'Ok',
                      handler: () => {
                        this.promptHandler.save(CHANGE_ALIAS);
                      }
                    }]
         });
      alert.present();  
    }
  }

  getName() {
    var nameField = this.article.get('name').value;

    if(nameField.length > 0) {
      return nameField;
    }

    return this.userName;
  }

  showChangeAliasPrompt() {
    return !this.promptHandler.isDoneFor(CHANGE_ALIAS);
  }

}
