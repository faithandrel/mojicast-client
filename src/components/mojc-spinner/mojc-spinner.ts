import { Component, Input } from '@angular/core';

@Component({
  selector: 'mojc-spinner',
  templateUrl: 'mojc-spinner.html'
})
export class SpinnerComponent {

	spinColor: string = 'white';
 
	@Input() showSpinner: boolean;
	@Input()
	set spinnerColor(spinnerColor: string) {
		if(spinnerColor.length > 0) {
			this.spinColor = spinnerColor;
		}
	}

	constructor() {

	}

}
